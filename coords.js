const axios = require("axios");

const API_KEY = "AIzaSyDyJPPlnIMOLp20Ef1LlTong8rYdTnaTXM";
const URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";

let getFormatedAddresAndCoords = async address => {
    let resp = await axios.get(getEncodedURL(address));

    if (resp.data.status === "ZERO_RESULTS") {
        throw new Error(`No results found for ${address}`);
    }

    let info = resp.data.results[0];
    return {
        formatedAddress: info.formatted_address,
        lat: info.geometry.location.lat,
        lng: info.geometry.location.lng
    };
};

let getEncodedURL = address => {
    return `${URL}${encodeURI(address)}&key=${API_KEY}`;
};

module.exports = {
    getFormatedAddresAndCoords
};