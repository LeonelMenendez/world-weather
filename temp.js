const axios = require('axios');

const API_KEY = 'zNkfutT17cnGTz49OIZUyg3pRmPRYZke';
const URL_LOCATION_KEY = `http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=${API_KEY}&q=`;
const URL_TEMP = `http://dataservice.accuweather.com/currentconditions/v1/`;

let getTemp = async(lat, lng) => {
    let resp = await axios.get(getURLLocationKey(lat, lng));
    resp = await axios.get(getURLTemp(resp.data.Key));

    return resp.data[0].Temperature.Metric.Value;
}

let getURLLocationKey = (lat, lng) => {
    let encodedURL = encodeURI(`${lat},${lng}`);
    return `${URL_LOCATION_KEY}${encodedURL})`;
}

let getURLTemp = locationKey => {
    return `${URL_TEMP}${locationKey}?apikey=${API_KEY}`;
}

module.exports = {
    getTemp
}