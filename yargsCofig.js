const argv = require('yargs').options({
    address: {
        alias: 'a',
        desc: 'Address of the city to obtain the weather.',
        demand: true
    }
}).argv;

module.exports = {
    argv
}