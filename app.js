const argv = require("./yargsCofig").argv;
const coords = require("./coords");
const temp = require("./temp");

let getTemperature = async address => {
    try {
        let location = await coords.getFormatedAddresAndCoords(address);
        let value = await temp.getTemp(location.lat, location.lng);

        return `The current temperature of the city ${location.formatedAddress} is ${value}º`;
    } catch (err) {
        throw new Error(`It wasn't possible to determine the temperature in ${address}`);
    }
};

getTemperature(argv.address)
    .then(temp => console.log(temp.green))
    .catch(err => console.log(err.red));